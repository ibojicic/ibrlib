#' Title
#'
#' @param mydb
#' @param query
#' @param tofactors
#'
#' @return
#' @export
#'
#' @examples
load_sql <- function(mydb,query,tofactors = NA) {
  options(warn=-1)
  rs <- dbSendQuery(mydb, query)
  data <- fetch(rs, n=-1)
  if (!is.na(tofactors)) {
    data[tofactors] <- lapply(data[tofactors], factor)
  }
  options(warn=0)
  data
}
